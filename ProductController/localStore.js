export let localStoreController = {
  setStore: (content, storeName) => {
    let sContent = JSON.stringify(content);
    localStorage.setItem(storeName, sContent);
  },
  getStore: (storeName) => {
    let output;
    if (localStorage.getItem(storeName)) {
      output = JSON.parse(localStorage.getItem(storeName));
    }
    return output;
  },
};
