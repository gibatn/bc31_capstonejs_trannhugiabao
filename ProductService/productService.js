export let productService = {
  getProduct: () => {
    return axios({
      url: "https://62b3cf60a36f3a973d271421.mockapi.io/product",
      method: "GET",
    });
  },
};
